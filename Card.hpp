#ifndef Card_H
#define Card_H

#include <string>
#include <sstream>
#include <iostream>
#include <vector>

class Card {
    public:
        int id;
        int size;
        int** bingoSpaces;
        Card();
        ~Card();
        void createCard(int numberMax, int cardSize, int id);
        std::string toString();
    private:
    
};


#endif