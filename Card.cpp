#include "Card.hpp"


Card::Card(){
    id = -1;
}

Card::~Card(){
    for(int i = 0; i < size; i++){
        delete [] bingoSpaces[i];
        bingoSpaces[i] = nullptr;
    }
    delete [] bingoSpaces;
    bingoSpaces = nullptr;
}

void Card::createCard(int numberMax, int cardSize, int cardId){
    id = cardId;
    size = cardSize;
    bingoSpaces = new int*[cardSize];
    for(int i = 0; i < cardSize; i++){
        bingoSpaces[i] = new int[cardSize];
        std::vector<int> used;
        for(int j = 0; j < cardSize; j++){
            int temp = rand() % numberMax;
            bool loop = true;
            if(used.size() != 0){
                while(loop){
                    for(unsigned int z=0; z < used.size(); z++){
                        bool found = false;
                        if(used[z] == temp){
                            found = true;
                        }
                        if(z == (used.size() - 1) && !found){
                            loop = false;
                        }
                    }
                    temp = rand() % numberMax;
                }
            }
            used.push_back(temp);
            bingoSpaces[i][j] = temp;
        }
    }
}


std::string Card::toString(){
    std::stringstream ss;
    for(int i = 0; i < size; i++){
        for(int j = 0; j < size; j++){
            ss << "[" << bingoSpaces[i][j] << "]";
        }
        ss << std::endl;
    }
    ss << std::endl;
    return ss.str();
}