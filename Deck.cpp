//
// Created by Stephen Clyde on 2/16/17.
//

#include "Deck.hpp"

Deck::Deck(int cardSize, int cardCount, int numberMax)
{
    deckSize = cardCount;
    deckOfCards = new Card[cardCount];
    for(int i=0; i < cardCount; i++){
        deckOfCards[i].createCard(numberMax, cardSize, i);
    }
}

Deck::~Deck()
{
    delete [] deckOfCards;
    deckOfCards = nullptr;
}

void Deck::print(std::ostream& out) const
{
    for(int i = 0; i < deckSize; i++){
        out << "Card ID: " << deckOfCards[i].id << std::endl;
        out << deckOfCards[i].toString();
    }
    out << std::endl;
}

void Deck::print(std::ostream& out, int cardIndex) const
{
    out << "Card ID: " <<deckOfCards[cardIndex].id << std::endl;
    out << deckOfCards[cardIndex].toString();
}



